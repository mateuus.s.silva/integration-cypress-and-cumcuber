
Given(/^que eu acesso a calculadora$/, () => {
    cy.visit('/')
});



When(/^Desejo realizar uma "([^"]*)"$/, (operacaoDesejada) => {

    let operador;

    switch(operacaoDesejada){
        case 'soma':
            operador = '+'
            break;
        case 'subtração':
            operador = '-'
            break;
        case 'multiplicação':
            operador = 'x'
            break;
        case 'divisao':
            operador = "÷"

        default:
        break;
    }


	Cypress.env('operador', operador)
});



When(/^informar os valores "([^"]*)" e "([^"]*)"$/, (primeiroValor,segundoValor) => {
	cy.get('div[class="button"], .button.zero').as('valores');
    cy.get('.operator').as('operadores');

    cy.get('@valores').contains(primeiroValor).click()
    cy.get('@operadores').contains(`${Cypress.env('operador')}`).click()
    cy.get('@valores').contains(segundoValor).click()
});

When(/^finalizar a conta$/, () => {
	return true;
});

Then(/^devo obter o resultado "([^"]*)"$/, (args1) => {
	console.log(args1);
	return true;
});
